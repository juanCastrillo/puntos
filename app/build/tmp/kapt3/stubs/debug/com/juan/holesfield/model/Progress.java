package com.juan.holesfield.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0001\u0018\u0000 \n2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t\u00a8\u0006\u000b"}, d2 = {"Lcom/juan/holesfield/model/Progress;", "", "(Ljava/lang/String;I)V", "puntos", "", "NotStarted", "Iniciated", "Advanced", "Close", "Unknown", "Companion", "app_debug"})
public enum Progress {
    /*public static final*/ NotStarted /* = new @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"Lcom/juan/holesfield/model/Progress$NotStarted;", "Lcom/juan/holesfield/model/Progress;", "puntos", "", "app_debug"}) NotStarted(){
        
        @java.lang.Override()
        public int puntos() {
            return 0;
        }
        
        NotStarted() {
            super();
        }
    } */,
    /*public static final*/ Iniciated /* = new @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"Lcom/juan/holesfield/model/Progress$Iniciated;", "Lcom/juan/holesfield/model/Progress;", "puntos", "", "app_debug"}) Iniciated(){
        
        @java.lang.Override()
        public int puntos() {
            return 0;
        }
        
        Iniciated() {
            super();
        }
    } */,
    /*public static final*/ Advanced /* = new @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"Lcom/juan/holesfield/model/Progress$Advanced;", "Lcom/juan/holesfield/model/Progress;", "puntos", "", "app_debug"}) Advanced(){
        
        @java.lang.Override()
        public int puntos() {
            return 0;
        }
        
        Advanced() {
            super();
        }
    } */,
    /*public static final*/ Close /* = new @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"Lcom/juan/holesfield/model/Progress$Close;", "Lcom/juan/holesfield/model/Progress;", "puntos", "", "app_debug"}) Close(){
        
        @java.lang.Override()
        public int puntos() {
            return 0;
        }
        
        Close() {
            super();
        }
    } */,
    /*public static final*/ Unknown /* = new @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u00c6\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"}, d2 = {"Lcom/juan/holesfield/model/Progress$Unknown;", "Lcom/juan/holesfield/model/Progress;", "puntos", "", "app_debug"}) Unknown(){
        
        @java.lang.Override()
        public int puntos() {
            return 0;
        }
        
        Unknown() {
            super();
        }
    } */;
    public static final com.juan.holesfield.model.Progress.Companion Companion = null;
    
    public abstract int puntos();
    
    Progress() {
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\b\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0004\u00a8\u0006\u000b"}, d2 = {"Lcom/juan/holesfield/model/Progress$Companion;", "", "()V", "colorProgress", "", "progress", "Lcom/juan/holesfield/model/Progress;", "mas", "menos", "puntosToProgress", "puntos", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.juan.holesfield.model.Progress puntosToProgress(int puntos) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.juan.holesfield.model.Progress mas(@org.jetbrains.annotations.NotNull()
        com.juan.holesfield.model.Progress progress) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.juan.holesfield.model.Progress menos(@org.jetbrains.annotations.NotNull()
        com.juan.holesfield.model.Progress progress) {
            return null;
        }
        
        public final int colorProgress(@org.jetbrains.annotations.NotNull()
        com.juan.holesfield.model.Progress progress) {
            return 0;
        }
        
        private Companion() {
            super();
        }
    }
}