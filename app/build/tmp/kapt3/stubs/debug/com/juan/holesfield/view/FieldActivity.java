package com.juan.holesfield.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\b\u0010D\u001a\u00020EH\u0002J\b\u0010F\u001a\u00020EH\u0002J\b\u0010G\u001a\u00020EH\u0002J\b\u0010H\u001a\u00020EH\u0002J\b\u0010I\u001a\u00020EH\u0002J\u0014\u0010J\u001a\u00020E2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020M0LJ\b\u0010N\u001a\u00020EH\u0016J\u0010\u0010O\u001a\u00020E2\u0006\u0010P\u001a\u00020\u0007H\u0016J\u0012\u0010Q\u001a\u00020E2\b\u0010R\u001a\u0004\u0018\u00010SH\u0014J \u0010T\u001a\u00020E2\u0006\u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020X2\u0006\u0010Y\u001a\u00020XH\u0016J\u000e\u0010Z\u001a\u00020E2\u0006\u0010[\u001a\u00020\\J\b\u0010]\u001a\u00020EH\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000eR\u001a\u0010\u0012\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\f\"\u0004\b\u0014\u0010\u000eR\u001a\u0010\u0015\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0017\u0010\u000eR\u001a\u0010\u0018\u001a\u00020\u0019X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010$\u001a\u00020%X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u000e\u0010*\u001a\u00020+X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082.\u00a2\u0006\u0002\n\u0000R\u0019\u0010.\u001a\n 0*\u0004\u0018\u00010/0/\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u000e\u00103\u001a\u000204X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u000206X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u00107\u001a\u00020%X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\'\"\u0004\b9\u0010)R\u001a\u0010:\u001a\u00020;X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u000e\u0010@\u001a\u00020AX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006^"}, d2 = {"Lcom/juan/holesfield/view/FieldActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/juan/holesfield/view/RecyclerItemTouchHelper$RecyclerItemTouchHelperListener;", "Landroid/view/View$OnClickListener;", "()V", "bottomSheetBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroid/view/View;", "bottomSheetCreateHole", "cardAdvanced", "Landroidx/cardview/widget/CardView;", "getCardAdvanced", "()Landroidx/cardview/widget/CardView;", "setCardAdvanced", "(Landroidx/cardview/widget/CardView;)V", "cardClose", "getCardClose", "setCardClose", "cardInitiated", "getCardInitiated", "setCardInitiated", "cardNotStarted", "getCardNotStarted", "setCardNotStarted", "crearTareaFull", "Landroid/widget/FrameLayout;", "getCrearTareaFull", "()Landroid/widget/FrameLayout;", "setCrearTareaFull", "(Landroid/widget/FrameLayout;)V", "expanded", "", "getExpanded", "()Z", "setExpanded", "(Z)V", "extraTarea", "Lcom/google/android/material/textfield/TextInputEditText;", "getExtraTarea", "()Lcom/google/android/material/textfield/TextInputEditText;", "setExtraTarea", "(Lcom/google/android/material/textfield/TextInputEditText;)V", "fieldHoles", "Landroidx/recyclerview/widget/RecyclerView;", "floatingButton", "Lcom/google/android/material/floatingactionbutton/FloatingActionButton;", "fm", "Landroidx/fragment/app/FragmentManager;", "kotlin.jvm.PlatformType", "getFm", "()Landroidx/fragment/app/FragmentManager;", "holesViewModel", "Lcom/juan/holesfield/viewmodel/HolesViewModel;", "inicioAdapter", "Lcom/juan/holesfield/view/FieldAdapter;", "nombreTarea", "getNombreTarea", "setNombreTarea", "progress", "Lcom/juan/holesfield/model/Progress;", "getProgress", "()Lcom/juan/holesfield/model/Progress;", "setProgress", "(Lcom/juan/holesfield/model/Progress;)V", "topDate", "Landroid/widget/TextView;", "topImage", "Landroid/widget/ImageView;", "allButtonsGrey", "", "animateThis", "floatingButtonClick", "initActivityViews", "initBottomSheetViews", "mostrarHoles", "holes", "", "Lcom/juan/holesfield/model/Hole;", "onBackPressed", "onClick", "v", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onSwiped", "viewHolder", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "direction", "", "position", "openHole", "id", "", "resetBottomSheet", "app_debug"})
public final class FieldActivity extends androidx.appcompat.app.AppCompatActivity implements com.juan.holesfield.view.RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, android.view.View.OnClickListener {
    private android.widget.ImageView topImage;
    private android.widget.TextView topDate;
    private androidx.recyclerview.widget.RecyclerView fieldHoles;
    private com.google.android.material.floatingactionbutton.FloatingActionButton floatingButton;
    private android.view.View bottomSheetCreateHole;
    private com.google.android.material.bottomsheet.BottomSheetBehavior<android.view.View> bottomSheetBehavior;
    @org.jetbrains.annotations.NotNull()
    public android.widget.FrameLayout crearTareaFull;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView cardNotStarted;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView cardInitiated;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView cardAdvanced;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView cardClose;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.textfield.TextInputEditText nombreTarea;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.textfield.TextInputEditText extraTarea;
    private boolean expanded;
    @org.jetbrains.annotations.NotNull()
    private com.juan.holesfield.model.Progress progress;
    private final androidx.fragment.app.FragmentManager fm = null;
    private com.juan.holesfield.view.FieldAdapter inicioAdapter;
    private com.juan.holesfield.viewmodel.HolesViewModel holesViewModel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.FrameLayout getCrearTareaFull() {
        return null;
    }
    
    public final void setCrearTareaFull(@org.jetbrains.annotations.NotNull()
    android.widget.FrameLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getCardNotStarted() {
        return null;
    }
    
    public final void setCardNotStarted(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getCardInitiated() {
        return null;
    }
    
    public final void setCardInitiated(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getCardAdvanced() {
        return null;
    }
    
    public final void setCardAdvanced(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getCardClose() {
        return null;
    }
    
    public final void setCardClose(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.textfield.TextInputEditText getNombreTarea() {
        return null;
    }
    
    public final void setNombreTarea(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputEditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.textfield.TextInputEditText getExtraTarea() {
        return null;
    }
    
    public final void setExtraTarea(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputEditText p0) {
    }
    
    public final boolean getExpanded() {
        return false;
    }
    
    public final void setExpanded(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.model.Progress getProgress() {
        return null;
    }
    
    public final void setProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress p0) {
    }
    
    public final androidx.fragment.app.FragmentManager getFm() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initActivityViews() {
    }
    
    private final void initBottomSheetViews() {
    }
    
    private final void floatingButtonClick() {
    }
    
    private final void resetBottomSheet() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    private final void allButtonsGrey() {
    }
    
    public final void openHole(long id) {
    }
    
    private final void animateThis() {
    }
    
    /**
     * * swipe on the hole to delete it
     */
    @java.lang.Override()
    public void onSwiped(@org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int direction, int position) {
    }
    
    public final void mostrarHoles(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> holes) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public FieldActivity() {
        super();
    }
}