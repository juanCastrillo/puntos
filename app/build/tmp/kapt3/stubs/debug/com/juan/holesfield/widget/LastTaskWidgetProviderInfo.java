package com.juan.holesfield.widget;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"}, d2 = {"Lcom/juan/holesfield/widget/LastTaskWidgetProviderInfo;", "Landroid/appwidget/AppWidgetProviderInfo;", "()V", "describeContents", "", "app_debug"})
public final class LastTaskWidgetProviderInfo extends android.appwidget.AppWidgetProviderInfo {
    
    @java.lang.Override()
    public int describeContents() {
        return 0;
    }
    
    public LastTaskWidgetProviderInfo() {
        super();
    }
}