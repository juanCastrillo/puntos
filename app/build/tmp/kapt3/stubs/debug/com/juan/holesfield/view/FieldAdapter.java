package com.juan.holesfield.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001)B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u001bH\u0016J\u0018\u0010 \u001a\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001bH\u0016J\u0018\u0010$\u001a\u00020\u00152\u0006\u0010%\u001a\u00020\u001b2\u0006\u0010&\u001a\u00020\u001bH\u0016J\u0014\u0010\'\u001a\u00020\u001d2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u00020\u0015X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019\u00a8\u0006*"}, d2 = {"Lcom/juan/holesfield/view/FieldAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/juan/holesfield/view/FieldAdapter$FieldViewHolder;", "Lcom/juan/holesfield/view/ItemTouchHelperAdapter;", "context", "Landroid/content/Context;", "listener", "Lcom/juan/holesfield/view/clicky;", "(Landroid/content/Context;Lcom/juan/holesfield/view/clicky;)V", "getContext", "()Landroid/content/Context;", "holes", "", "Lcom/juan/holesfield/model/Hole;", "getHoles", "()Ljava/util/List;", "setHoles", "(Ljava/util/List;)V", "getListener", "()Lcom/juan/holesfield/view/clicky;", "moving", "", "getMoving", "()Z", "setMoving", "(Z)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "onItemMove", "fromPosition", "toPosition", "updateList", "it", "FieldViewHolder", "app_debug"})
public final class FieldAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.juan.holesfield.view.FieldAdapter.FieldViewHolder> implements com.juan.holesfield.view.ItemTouchHelperAdapter {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<com.juan.holesfield.model.Hole> holes;
    private boolean moving;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.juan.holesfield.view.clicky listener = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.juan.holesfield.model.Hole> getHoles() {
        return null;
    }
    
    public final void setHoles(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> p0) {
    }
    
    public final boolean getMoving() {
        return false;
    }
    
    public final void setMoving(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.juan.holesfield.view.FieldAdapter.FieldViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.view.FieldAdapter.FieldViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean onItemMove(int fromPosition, int toPosition) {
        return false;
    }
    
    public final void updateList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> it) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.view.clicky getListener() {
        return null;
    }
    
    public FieldAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.juan.holesfield.view.clicky listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u0004R\u001a\u0010\u001b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\b\"\u0004\b\u001d\u0010\nR\u001a\u0010\u001e\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010$\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010!\"\u0004\b&\u0010#R\u001a\u0010\'\u001a\u00020\u001fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010!\"\u0004\b)\u0010#R\u001a\u0010*\u001a\u00020+X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u001a\u00100\u001a\u000201X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u00103\"\u0004\b4\u00105\u00a8\u00066"}, d2 = {"Lcom/juan/holesfield/view/FieldAdapter$FieldViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "backgroundHole", "Landroid/widget/LinearLayout;", "getBackgroundHole", "()Landroid/widget/LinearLayout;", "setBackgroundHole", "(Landroid/widget/LinearLayout;)V", "completHole", "Landroid/widget/FrameLayout;", "getCompletHole", "()Landroid/widget/FrameLayout;", "setCompletHole", "(Landroid/widget/FrameLayout;)V", "expandButton", "Landroid/widget/ImageView;", "getExpandButton", "()Landroid/widget/ImageView;", "setExpandButton", "(Landroid/widget/ImageView;)V", "extraProgress", "getExtraProgress", "()Landroid/view/View;", "setExtraProgress", "foregroundHole", "getForegroundHole", "setForegroundHole", "holeDate", "Landroid/widget/TextView;", "getHoleDate", "()Landroid/widget/TextView;", "setHoleDate", "(Landroid/widget/TextView;)V", "holeExtra", "getHoleExtra", "setHoleExtra", "holeName", "getHoleName", "setHoleName", "progress", "Landroid/widget/RelativeLayout;", "getProgress", "()Landroid/widget/RelativeLayout;", "setProgress", "(Landroid/widget/RelativeLayout;)V", "progressChip", "Lcom/juan/holesfield/view/ProgressChip;", "getProgressChip", "()Lcom/juan/holesfield/view/ProgressChip;", "setProgressChip", "(Lcom/juan/holesfield/view/ProgressChip;)V", "app_debug"})
    public static final class FieldViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private android.widget.FrameLayout completHole;
        @org.jetbrains.annotations.NotNull()
        private android.widget.LinearLayout foregroundHole;
        @org.jetbrains.annotations.NotNull()
        private android.widget.LinearLayout backgroundHole;
        @org.jetbrains.annotations.NotNull()
        private android.widget.TextView holeName;
        @org.jetbrains.annotations.NotNull()
        private android.widget.TextView holeExtra;
        @org.jetbrains.annotations.NotNull()
        private com.juan.holesfield.view.ProgressChip progressChip;
        @org.jetbrains.annotations.NotNull()
        private android.view.View extraProgress;
        @org.jetbrains.annotations.NotNull()
        private android.widget.TextView holeDate;
        @org.jetbrains.annotations.NotNull()
        private android.widget.RelativeLayout progress;
        @org.jetbrains.annotations.NotNull()
        private android.widget.ImageView expandButton;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.FrameLayout getCompletHole() {
            return null;
        }
        
        public final void setCompletHole(@org.jetbrains.annotations.NotNull()
        android.widget.FrameLayout p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getForegroundHole() {
            return null;
        }
        
        public final void setForegroundHole(@org.jetbrains.annotations.NotNull()
        android.widget.LinearLayout p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getBackgroundHole() {
            return null;
        }
        
        public final void setBackgroundHole(@org.jetbrains.annotations.NotNull()
        android.widget.LinearLayout p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getHoleName() {
            return null;
        }
        
        public final void setHoleName(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getHoleExtra() {
            return null;
        }
        
        public final void setHoleExtra(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.juan.holesfield.view.ProgressChip getProgressChip() {
            return null;
        }
        
        public final void setProgressChip(@org.jetbrains.annotations.NotNull()
        com.juan.holesfield.view.ProgressChip p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getExtraProgress() {
            return null;
        }
        
        public final void setExtraProgress(@org.jetbrains.annotations.NotNull()
        android.view.View p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getHoleDate() {
            return null;
        }
        
        public final void setHoleDate(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.RelativeLayout getProgress() {
            return null;
        }
        
        public final void setProgress(@org.jetbrains.annotations.NotNull()
        android.widget.RelativeLayout p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getExpandButton() {
            return null;
        }
        
        public final void setExpandButton(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        public FieldViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}