package com.juan.holesfield.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0012\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000f0\u000eJ\u0012\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000f0\u000eJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000f0\u000eJ\u0006\u0010\u0012\u001a\u00020\u0013J\u0014\u0010\u0014\u001a\u00020\n2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\f0\u000fJ\u000e\u0010\u0016\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\u0017\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0018"}, d2 = {"Lcom/juan/holesfield/viewmodel/HolesViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "repository", "Lcom/juan/holesfield/data/HolesRepository;", "(Landroid/app/Application;Lcom/juan/holesfield/data/HolesRepository;)V", "getRepository", "()Lcom/juan/holesfield/data/HolesRepository;", "addHole", "", "hole", "Lcom/juan/holesfield/model/Hole;", "getHolesCustom", "Landroidx/lifecycle/LiveData;", "", "getHolesDate", "getHolesProgress", "getOrder", "", "reOrderIndexes", "holesList", "removeHole", "updateProgress", "app_debug"})
public final class HolesViewModel extends androidx.lifecycle.AndroidViewModel {
    @org.jetbrains.annotations.NotNull()
    private final com.juan.holesfield.data.HolesRepository repository = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesProgress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesCustom() {
        return null;
    }
    
    public final int getOrder() {
        return 0;
    }
    
    public final void reOrderIndexes(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> holesList) {
    }
    
    public final void removeHole(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    public final void updateProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    public final void addHole(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.data.HolesRepository getRepository() {
        return null;
    }
    
    public HolesViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application, @org.jetbrains.annotations.NotNull()
    com.juan.holesfield.data.HolesRepository repository) {
        super(null);
    }
}