package com.juan.holesfield.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020\fJ\u0006\u0010\r\u001a\u00020\fJ\u000e\u0010\u000e\u001a\n \u0007*\u0004\u0018\u00010\f0\fR\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\b\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u0010"}, d2 = {"Lcom/juan/holesfield/utils/DateUtils;", "", "ct", "Landroid/content/Context;", "(Landroid/content/Context;)V", "format", "Ljava/time/format/DateTimeFormatter;", "kotlin.jvm.PlatformType", "holesFormat", "getHolesFormat", "()Ljava/time/format/DateTimeFormatter;", "fechaTextFormat", "", "fechaTextNoFormat", "holesDateNow", "Companion", "app_debug"})
public final class DateUtils {
    private final java.time.format.DateTimeFormatter format = null;
    private final java.time.format.DateTimeFormatter holesFormat = null;
    private static final java.time.format.DateTimeFormatter archiveFormatter = null;
    public static final com.juan.holesfield.utils.DateUtils.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String fechaTextNoFormat() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String fechaTextFormat() {
        return null;
    }
    
    public final java.time.format.DateTimeFormatter getHolesFormat() {
        return null;
    }
    
    public final java.lang.String holesDateNow() {
        return null;
    }
    
    public DateUtils(@org.jetbrains.annotations.NotNull()
    android.content.Context ct) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u0019\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\f"}, d2 = {"Lcom/juan/holesfield/utils/DateUtils$Companion;", "", "()V", "archiveFormatter", "Ljava/time/format/DateTimeFormatter;", "kotlin.jvm.PlatformType", "getArchiveFormatter", "()Ljava/time/format/DateTimeFormatter;", "daysPassed", "", "date", "Ljava/time/LocalDate;", "app_debug"})
    public static final class Companion {
        
        public final int daysPassed(@org.jetbrains.annotations.NotNull()
        java.time.LocalDate date) {
            return 0;
        }
        
        public final java.time.format.DateTimeFormatter getArchiveFormatter() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}