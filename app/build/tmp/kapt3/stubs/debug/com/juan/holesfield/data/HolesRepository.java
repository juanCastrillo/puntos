package com.juan.holesfield.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\r0\fJ\u0012\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\r0\fJ\u0012\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\r0\fJ\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0012\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0014\u0010\u0013\u001a\u00020\b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\rJ\u000e\u0010\u0015\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0016"}, d2 = {"Lcom/juan/holesfield/data/HolesRepository;", "", "holesDao", "Lcom/juan/holesfield/storage/dao/HolesDao;", "(Lcom/juan/holesfield/storage/dao/HolesDao;)V", "getHolesDao", "()Lcom/juan/holesfield/storage/dao/HolesDao;", "deleteHole", "", "hole", "Lcom/juan/holesfield/model/Hole;", "getHolesCustom", "Landroidx/lifecycle/LiveData;", "", "getHolesDate", "getHolesProgress", "getOrder", "", "putHoles", "updateIndex", "lista", "updateProgress", "app_debug"})
public final class HolesRepository {
    @org.jetbrains.annotations.NotNull()
    private final com.juan.holesfield.storage.dao.HolesDao holesDao = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesProgress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesCustom() {
        return null;
    }
    
    public final int getOrder() {
        return 0;
    }
    
    public final void deleteHole(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    public final void updateIndex(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> lista) {
    }
    
    public final void updateProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    public final void putHoles(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.storage.dao.HolesDao getHolesDao() {
        return null;
    }
    
    public HolesRepository(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.storage.dao.HolesDao holesDao) {
        super();
    }
}