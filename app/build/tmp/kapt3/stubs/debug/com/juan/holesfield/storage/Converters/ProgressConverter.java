package com.juan.holesfield.storage.Converters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0004H\u0007\u00a8\u0006\t"}, d2 = {"Lcom/juan/holesfield/storage/Converters/ProgressConverter;", "", "()V", "convertToDatabaseValue", "", "progress", "Lcom/juan/holesfield/model/Progress;", "convertToEntityProperty", "puntos", "app_debug"})
public final class ProgressConverter {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.TypeConverter()
    public final com.juan.holesfield.model.Progress convertToEntityProperty(int puntos) {
        return null;
    }
    
    @androidx.room.TypeConverter()
    public final int convertToDatabaseValue(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress progress) {
        return 0;
    }
    
    public ProgressConverter() {
        super();
    }
}