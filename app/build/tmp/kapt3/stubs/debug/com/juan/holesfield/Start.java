package com.juan.holesfield;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0017"}, d2 = {"Lcom/juan/holesfield/Start;", "Landroid/app/Application;", "()V", "holesDatabase", "Lcom/juan/holesfield/storage/database/HolesDatabase;", "getHolesDatabase", "()Lcom/juan/holesfield/storage/database/HolesDatabase;", "setHolesDatabase", "(Lcom/juan/holesfield/storage/database/HolesDatabase;)V", "holesRepository", "Lcom/juan/holesfield/data/HolesRepository;", "getHolesRepository", "()Lcom/juan/holesfield/data/HolesRepository;", "setHolesRepository", "(Lcom/juan/holesfield/data/HolesRepository;)V", "user", "Lcom/google/firebase/auth/FirebaseUser;", "getUser", "()Lcom/google/firebase/auth/FirebaseUser;", "setUser", "(Lcom/google/firebase/auth/FirebaseUser;)V", "onCreate", "", "app_debug"})
public final class Start extends android.app.Application {
    @org.jetbrains.annotations.NotNull()
    public com.juan.holesfield.data.HolesRepository holesRepository;
    @org.jetbrains.annotations.NotNull()
    public com.juan.holesfield.storage.database.HolesDatabase holesDatabase;
    @org.jetbrains.annotations.Nullable()
    private com.google.firebase.auth.FirebaseUser user;
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.data.HolesRepository getHolesRepository() {
        return null;
    }
    
    public final void setHolesRepository(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.data.HolesRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.storage.database.HolesDatabase getHolesDatabase() {
        return null;
    }
    
    public final void setHolesDatabase(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.storage.database.HolesDatabase p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.firebase.auth.FirebaseUser getUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.Nullable()
    com.google.firebase.auth.FirebaseUser p0) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    public Start() {
        super();
    }
}