package com.juan.holesfield.view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 <2\u00020\u00012\u00020\u0002:\u0001<B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u000101H\u0016J&\u00102\u001a\u0004\u0018\u0001012\u0006\u00103\u001a\u0002042\b\u00105\u001a\u0004\u0018\u0001062\b\u00107\u001a\u0004\u0018\u000108H\u0016J\b\u00109\u001a\u00020/H\u0002J\u0006\u0010:\u001a\u00020;R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0013\"\u0004\b\u0018\u0010\u0015R\u001a\u0010\u0019\u001a\u00020\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u0013\"\u0004\b\u001b\u0010\u0015R\u001a\u0010\u001c\u001a\u00020\u0011X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u0013\"\u0004\b\u001e\u0010\u0015R\u001a\u0010\u001f\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001a\u0010%\u001a\u00020&X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010(\"\u0004\b)\u0010*R\u001a\u0010+\u001a\u00020 X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\"\"\u0004\b-\u0010$\u00a8\u0006="}, d2 = {"Lcom/juan/holesfield/view/CreateHoleBottomFragment;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "Landroid/view/View$OnClickListener;", "()V", "botonCrearTarea", "Lcom/google/android/material/button/MaterialButton;", "getBotonCrearTarea", "()Lcom/google/android/material/button/MaterialButton;", "setBotonCrearTarea", "(Lcom/google/android/material/button/MaterialButton;)V", "crearTareaFull", "Landroid/widget/FrameLayout;", "getCrearTareaFull", "()Landroid/widget/FrameLayout;", "setCrearTareaFull", "(Landroid/widget/FrameLayout;)V", "difDif", "Landroidx/cardview/widget/CardView;", "getDifDif", "()Landroidx/cardview/widget/CardView;", "setDifDif", "(Landroidx/cardview/widget/CardView;)V", "difFac", "getDifFac", "setDifFac", "difImp", "getDifImp", "setDifImp", "difMed", "getDifMed", "setDifMed", "nombreTarea", "Lcom/google/android/material/textfield/TextInputEditText;", "getNombreTarea", "()Lcom/google/android/material/textfield/TextInputEditText;", "setNombreTarea", "(Lcom/google/android/material/textfield/TextInputEditText;)V", "progress", "Lcom/juan/holesfield/model/Progress;", "getProgress", "()Lcom/juan/holesfield/model/Progress;", "setProgress", "(Lcom/juan/holesfield/model/Progress;)V", "resumenTarea", "getResumenTarea", "setResumenTarea", "onClick", "", "v", "Landroid/view/View;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "saveTarea", "viewModel", "Lcom/juan/holesfield/viewmodel/HolesViewModel;", "Companion", "app_debug"})
public final class CreateHoleBottomFragment extends com.google.android.material.bottomsheet.BottomSheetDialogFragment implements android.view.View.OnClickListener {
    @org.jetbrains.annotations.NotNull()
    public android.widget.FrameLayout crearTareaFull;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView difFac;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView difMed;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView difDif;
    @org.jetbrains.annotations.NotNull()
    public androidx.cardview.widget.CardView difImp;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.textfield.TextInputEditText nombreTarea;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.textfield.TextInputEditText resumenTarea;
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.button.MaterialButton botonCrearTarea;
    @org.jetbrains.annotations.NotNull()
    private com.juan.holesfield.model.Progress progress;
    public static final com.juan.holesfield.view.CreateHoleBottomFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final android.widget.FrameLayout getCrearTareaFull() {
        return null;
    }
    
    public final void setCrearTareaFull(@org.jetbrains.annotations.NotNull()
    android.widget.FrameLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getDifFac() {
        return null;
    }
    
    public final void setDifFac(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getDifMed() {
        return null;
    }
    
    public final void setDifMed(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getDifDif() {
        return null;
    }
    
    public final void setDifDif(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.cardview.widget.CardView getDifImp() {
        return null;
    }
    
    public final void setDifImp(@org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.textfield.TextInputEditText getNombreTarea() {
        return null;
    }
    
    public final void setNombreTarea(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputEditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.textfield.TextInputEditText getResumenTarea() {
        return null;
    }
    
    public final void setResumenTarea(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputEditText p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.button.MaterialButton getBotonCrearTarea() {
        return null;
    }
    
    public final void setBotonCrearTarea(@org.jetbrains.annotations.NotNull()
    com.google.android.material.button.MaterialButton p0) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.model.Progress getProgress() {
        return null;
    }
    
    public final void setProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void saveTarea() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.viewmodel.HolesViewModel viewModel() {
        return null;
    }
    
    public CreateHoleBottomFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/juan/holesfield/view/CreateHoleBottomFragment$Companion;", "", "()V", "newInstance", "Lcom/juan/holesfield/view/CreateHoleBottomFragment;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.juan.holesfield.view.CreateHoleBottomFragment newInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}