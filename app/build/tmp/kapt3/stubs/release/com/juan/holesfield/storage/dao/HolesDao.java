package com.juan.holesfield.storage.dao;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007H\'J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007H\'J\u0014\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\b0\u0007H\'J\b\u0010\u000b\u001a\u00020\fH\'J!\u0010\r\u001a\u00020\u00032\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u000f\"\u00020\u0005H\'\u00a2\u0006\u0002\u0010\u0010J\u0016\u0010\u0011\u001a\u00020\u00032\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00050\bH\'J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0013"}, d2 = {"Lcom/juan/holesfield/storage/dao/HolesDao;", "", "deleteHole", "", "hole", "Lcom/juan/holesfield/model/Hole;", "getHolesByCustomOrder", "Landroidx/lifecycle/LiveData;", "", "getHolesByMoment", "getHolesByProgress", "getLastOrder", "", "putIn", "holes", "", "([Lcom/juan/holesfield/model/Hole;)V", "updateIndexes", "updateProgress", "app_release"})
public abstract interface HolesDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM holes order by progress")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesByProgress();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM holes order by moment")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesByMoment();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM holes order by custom_order")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.juan.holesfield.model.Hole>> getHolesByCustomOrder();
    
    @androidx.room.Query(value = "SELECT custom_order FROM holes order by custom_order DESC limit 1")
    public abstract int getLastOrder();
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void putIn(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole... holes);
    
    @androidx.room.Update()
    public abstract void updateIndexes(@org.jetbrains.annotations.NotNull()
    java.util.List<com.juan.holesfield.model.Hole> holes);
    
    @androidx.room.Update()
    public abstract void updateProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole);
    
    @androidx.room.Delete()
    public abstract void deleteHole(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Hole hole);
}