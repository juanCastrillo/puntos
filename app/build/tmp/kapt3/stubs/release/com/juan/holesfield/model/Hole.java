package com.juan.holesfield.model;

import java.lang.System;

@androidx.room.Entity(tableName = "holes")
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b-\b\u0087\b\u0018\u00002\u00020\u0001BK\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010.\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010 J\t\u0010/\u001a\u00020\u0005H\u00c6\u0003J\t\u00100\u001a\u00020\u0005H\u00c6\u0003J\t\u00101\u001a\u00020\bH\u00c6\u0003J\t\u00102\u001a\u00020\nH\u00c6\u0003J\t\u00103\u001a\u00020\fH\u00c6\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\t\u00105\u001a\u00020\u000fH\u00c6\u0003Jb\u00106\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000e\u001a\u00020\u000fH\u00c6\u0001\u00a2\u0006\u0002\u00107J\u0013\u00108\u001a\u00020\f2\b\u00109\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010:\u001a\u00020\u000fH\u00d6\u0001J\t\u0010;\u001a\u00020\u0005H\u00d6\u0001R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001c\u0010\r\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001a\"\u0004\b\u001e\u0010\u001cR\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010#\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0016\"\u0004\b%\u0010\u0018R\u001e\u0010\u000e\u001a\u00020\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-\u00a8\u0006<"}, d2 = {"Lcom/juan/holesfield/model/Hole;", "", "id", "", "hole", "", "extra", "progress", "Lcom/juan/holesfield/model/Progress;", "moment", "Ljava/time/LocalDate;", "completed", "", "completedMoment", "order", "", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/juan/holesfield/model/Progress;Ljava/time/LocalDate;ZLjava/time/LocalDate;I)V", "getCompleted", "()Z", "setCompleted", "(Z)V", "getCompletedMoment", "()Ljava/time/LocalDate;", "setCompletedMoment", "(Ljava/time/LocalDate;)V", "getExtra", "()Ljava/lang/String;", "setExtra", "(Ljava/lang/String;)V", "getHole", "setHole", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getMoment", "setMoment", "getOrder", "()I", "setOrder", "(I)V", "getProgress", "()Lcom/juan/holesfield/model/Progress;", "setProgress", "(Lcom/juan/holesfield/model/Progress;)V", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/juan/holesfield/model/Progress;Ljava/time/LocalDate;ZLjava/time/LocalDate;I)Lcom/juan/holesfield/model/Hole;", "equals", "other", "hashCode", "toString", "app_release"})
public final class Hole {
    @org.jetbrains.annotations.Nullable()
    @androidx.room.PrimaryKey()
    private java.lang.Long id;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String hole;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String extra;
    @org.jetbrains.annotations.NotNull()
    private com.juan.holesfield.model.Progress progress;
    @org.jetbrains.annotations.NotNull()
    private java.time.LocalDate moment;
    private boolean completed;
    @org.jetbrains.annotations.Nullable()
    private java.time.LocalDate completedMoment;
    @androidx.room.ColumnInfo(name = "custom_order")
    private int order;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getHole() {
        return null;
    }
    
    public final void setHole(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getExtra() {
        return null;
    }
    
    public final void setExtra(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.model.Progress getProgress() {
        return null;
    }
    
    public final void setProgress(@org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate getMoment() {
        return null;
    }
    
    public final void setMoment(@org.jetbrains.annotations.NotNull()
    java.time.LocalDate p0) {
    }
    
    public final boolean getCompleted() {
        return false;
    }
    
    public final void setCompleted(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate getCompletedMoment() {
        return null;
    }
    
    public final void setCompletedMoment(@org.jetbrains.annotations.Nullable()
    java.time.LocalDate p0) {
    }
    
    public final int getOrder() {
        return 0;
    }
    
    public final void setOrder(int p0) {
    }
    
    public Hole(@org.jetbrains.annotations.Nullable()
    java.lang.Long id, @org.jetbrains.annotations.NotNull()
    java.lang.String hole, @org.jetbrains.annotations.NotNull()
    java.lang.String extra, @org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress progress, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate moment, boolean completed, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate completedMoment, int order) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.model.Progress component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.time.LocalDate component5() {
        return null;
    }
    
    public final boolean component6() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.time.LocalDate component7() {
        return null;
    }
    
    public final int component8() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.juan.holesfield.model.Hole copy(@org.jetbrains.annotations.Nullable()
    java.lang.Long id, @org.jetbrains.annotations.NotNull()
    java.lang.String hole, @org.jetbrains.annotations.NotNull()
    java.lang.String extra, @org.jetbrains.annotations.NotNull()
    com.juan.holesfield.model.Progress progress, @org.jetbrains.annotations.NotNull()
    java.time.LocalDate moment, boolean completed, @org.jetbrains.annotations.Nullable()
    java.time.LocalDate completedMoment, int order) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}