package com.juan.holesfield.storage.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.juan.holesfield.storage.dao.HolesDao;
import com.juan.holesfield.storage.dao.HolesDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class HolesDatabase_Impl extends HolesDatabase {
  private volatile HolesDao _holesDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `holes` (`id` INTEGER, `hole` TEXT NOT NULL, `extra` TEXT NOT NULL, `progress` INTEGER NOT NULL, `moment` TEXT NOT NULL, `completed` INTEGER NOT NULL, `completedMoment` TEXT, `custom_order` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"45ab836077e23762b1e118ffb819ef17\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `holes`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsHoles = new HashMap<String, TableInfo.Column>(8);
        _columnsHoles.put("id", new TableInfo.Column("id", "INTEGER", false, 1));
        _columnsHoles.put("hole", new TableInfo.Column("hole", "TEXT", true, 0));
        _columnsHoles.put("extra", new TableInfo.Column("extra", "TEXT", true, 0));
        _columnsHoles.put("progress", new TableInfo.Column("progress", "INTEGER", true, 0));
        _columnsHoles.put("moment", new TableInfo.Column("moment", "TEXT", true, 0));
        _columnsHoles.put("completed", new TableInfo.Column("completed", "INTEGER", true, 0));
        _columnsHoles.put("completedMoment", new TableInfo.Column("completedMoment", "TEXT", false, 0));
        _columnsHoles.put("custom_order", new TableInfo.Column("custom_order", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysHoles = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesHoles = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoHoles = new TableInfo("holes", _columnsHoles, _foreignKeysHoles, _indicesHoles);
        final TableInfo _existingHoles = TableInfo.read(_db, "holes");
        if (! _infoHoles.equals(_existingHoles)) {
          throw new IllegalStateException("Migration didn't properly handle holes(com.juan.holesfield.model.Hole).\n"
                  + " Expected:\n" + _infoHoles + "\n"
                  + " Found:\n" + _existingHoles);
        }
      }
    }, "45ab836077e23762b1e118ffb819ef17", "65ae356a3f3a8f4fd63dc1e7b77bebc9");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "holes");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `holes`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public HolesDao holesDao() {
    if (_holesDao != null) {
      return _holesDao;
    } else {
      synchronized(this) {
        if(_holesDao == null) {
          _holesDao = new HolesDao_Impl(this);
        }
        return _holesDao;
      }
    }
  }
}
