package com.juan.holesfield.storage.dao;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.lifecycle.ComputableLiveData;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.InvalidationTracker.Observer;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.juan.holesfield.model.Hole;
import com.juan.holesfield.model.Progress;
import com.juan.holesfield.storage.Converters.LocalDateTimeConverter;
import com.juan.holesfield.storage.Converters.ProgressConverter;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public final class HolesDao_Impl implements HolesDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfHole;

  private final ProgressConverter __progressConverter = new ProgressConverter();

  private final LocalDateTimeConverter __localDateTimeConverter = new LocalDateTimeConverter();

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfHole;

  private final EntityDeletionOrUpdateAdapter __updateAdapterOfHole;

  public HolesDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfHole = new EntityInsertionAdapter<Hole>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `holes`(`id`,`hole`,`extra`,`progress`,`moment`,`completed`,`completedMoment`,`custom_order`) VALUES (?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Hole value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getHole() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getHole());
        }
        if (value.getExtra() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getExtra());
        }
        final int _tmp;
        _tmp = __progressConverter.convertToDatabaseValue(value.getProgress());
        stmt.bindLong(4, _tmp);
        final String _tmp_1;
        _tmp_1 = __localDateTimeConverter.LocalDateToString(value.getMoment());
        if (_tmp_1 == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, _tmp_1);
        }
        final int _tmp_2;
        _tmp_2 = value.getCompleted() ? 1 : 0;
        stmt.bindLong(6, _tmp_2);
        final String _tmp_3;
        _tmp_3 = __localDateTimeConverter.LocalDateToString(value.getCompletedMoment());
        if (_tmp_3 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_3);
        }
        stmt.bindLong(8, value.getOrder());
      }
    };
    this.__deletionAdapterOfHole = new EntityDeletionOrUpdateAdapter<Hole>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `holes` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Hole value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
      }
    };
    this.__updateAdapterOfHole = new EntityDeletionOrUpdateAdapter<Hole>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `holes` SET `id` = ?,`hole` = ?,`extra` = ?,`progress` = ?,`moment` = ?,`completed` = ?,`completedMoment` = ?,`custom_order` = ? WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Hole value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getHole() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getHole());
        }
        if (value.getExtra() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getExtra());
        }
        final int _tmp;
        _tmp = __progressConverter.convertToDatabaseValue(value.getProgress());
        stmt.bindLong(4, _tmp);
        final String _tmp_1;
        _tmp_1 = __localDateTimeConverter.LocalDateToString(value.getMoment());
        if (_tmp_1 == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, _tmp_1);
        }
        final int _tmp_2;
        _tmp_2 = value.getCompleted() ? 1 : 0;
        stmt.bindLong(6, _tmp_2);
        final String _tmp_3;
        _tmp_3 = __localDateTimeConverter.LocalDateToString(value.getCompletedMoment());
        if (_tmp_3 == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, _tmp_3);
        }
        stmt.bindLong(8, value.getOrder());
        if (value.getId() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindLong(9, value.getId());
        }
      }
    };
  }

  @Override
  public void putIn(Hole... holes) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfHole.insert(holes);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteHole(Hole hole) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfHole.handle(hole);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateIndexes(List<Hole> holes) {
    __db.beginTransaction();
    try {
      __updateAdapterOfHole.handleMultiple(holes);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void updateProgress(Hole hole) {
    __db.beginTransaction();
    try {
      __updateAdapterOfHole.handle(hole);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<Hole>> getHolesByProgress() {
    final String _sql = "SELECT * FROM holes order by progress";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Hole>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Hole> compute() {
        if (_observer == null) {
          _observer = new Observer("holes") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfHole = _cursor.getColumnIndexOrThrow("hole");
          final int _cursorIndexOfExtra = _cursor.getColumnIndexOrThrow("extra");
          final int _cursorIndexOfProgress = _cursor.getColumnIndexOrThrow("progress");
          final int _cursorIndexOfMoment = _cursor.getColumnIndexOrThrow("moment");
          final int _cursorIndexOfCompleted = _cursor.getColumnIndexOrThrow("completed");
          final int _cursorIndexOfCompletedMoment = _cursor.getColumnIndexOrThrow("completedMoment");
          final int _cursorIndexOfOrder = _cursor.getColumnIndexOrThrow("custom_order");
          final List<Hole> _result = new ArrayList<Hole>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Hole _item;
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            final String _tmpHole;
            _tmpHole = _cursor.getString(_cursorIndexOfHole);
            final String _tmpExtra;
            _tmpExtra = _cursor.getString(_cursorIndexOfExtra);
            final Progress _tmpProgress;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfProgress);
            _tmpProgress = __progressConverter.convertToEntityProperty(_tmp);
            final LocalDate _tmpMoment;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfMoment);
            _tmpMoment = __localDateTimeConverter.StringToLocalDate(_tmp_1);
            final boolean _tmpCompleted;
            final int _tmp_2;
            _tmp_2 = _cursor.getInt(_cursorIndexOfCompleted);
            _tmpCompleted = _tmp_2 != 0;
            final LocalDate _tmpCompletedMoment;
            final String _tmp_3;
            _tmp_3 = _cursor.getString(_cursorIndexOfCompletedMoment);
            _tmpCompletedMoment = __localDateTimeConverter.StringToLocalDate(_tmp_3);
            final int _tmpOrder;
            _tmpOrder = _cursor.getInt(_cursorIndexOfOrder);
            _item = new Hole(_tmpId,_tmpHole,_tmpExtra,_tmpProgress,_tmpMoment,_tmpCompleted,_tmpCompletedMoment,_tmpOrder);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Hole>> getHolesByMoment() {
    final String _sql = "SELECT * FROM holes order by moment";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Hole>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Hole> compute() {
        if (_observer == null) {
          _observer = new Observer("holes") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfHole = _cursor.getColumnIndexOrThrow("hole");
          final int _cursorIndexOfExtra = _cursor.getColumnIndexOrThrow("extra");
          final int _cursorIndexOfProgress = _cursor.getColumnIndexOrThrow("progress");
          final int _cursorIndexOfMoment = _cursor.getColumnIndexOrThrow("moment");
          final int _cursorIndexOfCompleted = _cursor.getColumnIndexOrThrow("completed");
          final int _cursorIndexOfCompletedMoment = _cursor.getColumnIndexOrThrow("completedMoment");
          final int _cursorIndexOfOrder = _cursor.getColumnIndexOrThrow("custom_order");
          final List<Hole> _result = new ArrayList<Hole>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Hole _item;
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            final String _tmpHole;
            _tmpHole = _cursor.getString(_cursorIndexOfHole);
            final String _tmpExtra;
            _tmpExtra = _cursor.getString(_cursorIndexOfExtra);
            final Progress _tmpProgress;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfProgress);
            _tmpProgress = __progressConverter.convertToEntityProperty(_tmp);
            final LocalDate _tmpMoment;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfMoment);
            _tmpMoment = __localDateTimeConverter.StringToLocalDate(_tmp_1);
            final boolean _tmpCompleted;
            final int _tmp_2;
            _tmp_2 = _cursor.getInt(_cursorIndexOfCompleted);
            _tmpCompleted = _tmp_2 != 0;
            final LocalDate _tmpCompletedMoment;
            final String _tmp_3;
            _tmp_3 = _cursor.getString(_cursorIndexOfCompletedMoment);
            _tmpCompletedMoment = __localDateTimeConverter.StringToLocalDate(_tmp_3);
            final int _tmpOrder;
            _tmpOrder = _cursor.getInt(_cursorIndexOfOrder);
            _item = new Hole(_tmpId,_tmpHole,_tmpExtra,_tmpProgress,_tmpMoment,_tmpCompleted,_tmpCompletedMoment,_tmpOrder);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public LiveData<List<Hole>> getHolesByCustomOrder() {
    final String _sql = "SELECT * FROM holes order by custom_order";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Hole>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Hole> compute() {
        if (_observer == null) {
          _observer = new Observer("holes") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfHole = _cursor.getColumnIndexOrThrow("hole");
          final int _cursorIndexOfExtra = _cursor.getColumnIndexOrThrow("extra");
          final int _cursorIndexOfProgress = _cursor.getColumnIndexOrThrow("progress");
          final int _cursorIndexOfMoment = _cursor.getColumnIndexOrThrow("moment");
          final int _cursorIndexOfCompleted = _cursor.getColumnIndexOrThrow("completed");
          final int _cursorIndexOfCompletedMoment = _cursor.getColumnIndexOrThrow("completedMoment");
          final int _cursorIndexOfOrder = _cursor.getColumnIndexOrThrow("custom_order");
          final List<Hole> _result = new ArrayList<Hole>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Hole _item;
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            final String _tmpHole;
            _tmpHole = _cursor.getString(_cursorIndexOfHole);
            final String _tmpExtra;
            _tmpExtra = _cursor.getString(_cursorIndexOfExtra);
            final Progress _tmpProgress;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfProgress);
            _tmpProgress = __progressConverter.convertToEntityProperty(_tmp);
            final LocalDate _tmpMoment;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfMoment);
            _tmpMoment = __localDateTimeConverter.StringToLocalDate(_tmp_1);
            final boolean _tmpCompleted;
            final int _tmp_2;
            _tmp_2 = _cursor.getInt(_cursorIndexOfCompleted);
            _tmpCompleted = _tmp_2 != 0;
            final LocalDate _tmpCompletedMoment;
            final String _tmp_3;
            _tmp_3 = _cursor.getString(_cursorIndexOfCompletedMoment);
            _tmpCompletedMoment = __localDateTimeConverter.StringToLocalDate(_tmp_3);
            final int _tmpOrder;
            _tmpOrder = _cursor.getInt(_cursorIndexOfOrder);
            _item = new Hole(_tmpId,_tmpHole,_tmpExtra,_tmpProgress,_tmpMoment,_tmpCompleted,_tmpCompletedMoment,_tmpOrder);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public int getLastOrder() {
    final String _sql = "SELECT custom_order FROM holes order by custom_order DESC limit 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _result;
      if(_cursor.moveToFirst()) {
        _result = _cursor.getInt(0);
      } else {
        _result = 0;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
