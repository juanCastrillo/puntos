package com.juan.holesfield.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.juan.holesfield.data.HolesRepository
import com.juan.holesfield.model.Hole

class HolesViewModel(application: Application, val repository: HolesRepository): AndroidViewModel(application) {

    fun getHolesDate() = repository.getHolesDate()
    fun getHolesProgress() = repository.getHolesProgress()
    fun getHolesCustom() = repository.getHolesCustom()

    fun getOrder() = repository.getOrder()

    fun reOrderIndexes(holesList:List<Hole>){
        Log.d("HolesViewModel", "list to order: $holesList")
        var index = 1
        holesList.forEach{
            it.order = index
            index ++
        }
        repository.updateIndex(holesList)
    }

    fun removeHole(hole:Hole){repository.deleteHole(hole)}

    fun updateProgress(hole:Hole) {repository.updateProgress(hole)}

    fun addHole(hole:Hole) {repository.putHoles(hole)}
}

class HolesFactory(val application: Application, val repository: HolesRepository):ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HolesViewModel(application, repository) as T
    }
}