package com.juan.holesfield

import android.app.Application
import android.content.Context
import androidx.core.content.ContextCompat
import androidx.room.Room
import androidx.room.RoomDatabase
import com.google.firebase.auth.FirebaseUser
import com.juan.holesfield.data.HolesRepository
import com.juan.holesfield.storage.database.HolesDatabase


class Start : Application() {

    lateinit var holesRepository: HolesRepository
    lateinit var holesDatabase:HolesDatabase
    var user:FirebaseUser? = null

    override fun onCreate() {
        super.onCreate()
        holesDatabase = Room.databaseBuilder(this, HolesDatabase::class.java, "holes_database").build()
        holesRepository = HolesRepository(holesDatabase.holesDao())

    }
}

