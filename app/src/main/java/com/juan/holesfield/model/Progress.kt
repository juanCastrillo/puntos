package com.juan.holesfield.model

import android.content.Context
import com.juan.holesfield.R

enum class Progress {
    NotStarted  { override fun puntos() = 1},
    Iniciated   { override fun puntos() = 2},
    Advanced    { override fun puntos() = 3},
    Close       { override fun puntos() = 4},
    Unknown     { override fun puntos() = 0};


    abstract fun puntos():Int
    companion object {

        fun puntosToProgress(puntos: Int): Progress = when (puntos) {
            1 -> NotStarted
            2 -> Iniciated
            3 -> Advanced
            4 -> Close
            else -> Unknown
        }

        fun mas(progress: Progress): Progress {
            val newProgress = puntosToProgress(progress.puntos() + 1)
            return if(newProgress == Unknown) progress
            else newProgress
        }

        fun menos(progress: Progress): Progress {
            val newProgress = puntosToProgress(progress.puntos() - 1)
            return if(newProgress == Unknown) progress
            else newProgress
        }

        fun colorProgress(progress: Progress):Int = when(progress) {
            NotStarted -> R.color.NotStarted
            Iniciated -> R.color.Iniciated
            Advanced -> R.color.Advanced
            Close -> R.color.Close
            Unknown -> R.color.Unknown
        }

    }
}

fun Progress.toString(ct: Context):String =
        ct.getString(when(this){
            Progress.NotStarted -> R.string.not_started
            Progress.Iniciated -> R.string.iniciated
            Progress.Advanced -> R.string.advanced
            Progress.Close -> R.string.close
            Progress.Unknown -> R.string.not_started
        })