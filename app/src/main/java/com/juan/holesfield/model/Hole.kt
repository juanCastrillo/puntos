package com.juan.holesfield.model


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*
import java.time.LocalDate

@Entity(tableName = "holes")
data class Hole(@PrimaryKey var id: Long?,

        //name of the hole
        var hole:String,

        //description or/and links to expand on name
        var extra: String,

        //Progress of the hole
        var progress: Progress,

        //creation date of the hole
        var moment: LocalDate,

        //Is the hole finished (is everything about the topic learnt) == true | false
        var completed:Boolean,

        //End date of the hole 
        var completedMoment:LocalDate? = null,

        //position of relevance
        @ColumnInfo(name = "custom_order") var order:Int
)




