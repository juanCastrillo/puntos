package com.juan.holesfield.data

import com.juan.holesfield.model.Hole
import com.juan.holesfield.storage.dao.HolesDao

class HolesRepository(val holesDao: HolesDao) {
    fun getHolesDate() = holesDao.getHolesByMoment()
    fun getHolesProgress() = holesDao.getHolesByProgress()
    fun getHolesCustom() = holesDao.getHolesByCustomOrder()

    fun getOrder() = holesDao.getLastOrder()

    fun deleteHole(hole:Hole) {holesDao.deleteHole(hole)}

    fun updateIndex(lista: List<Hole>) {
        holesDao.updateIndexes(lista)
    }

    fun updateProgress(hole:Hole){holesDao.updateProgress(hole)}

    fun putHoles(hole:Hole) {
        holesDao.putIn(hole)
    }
}