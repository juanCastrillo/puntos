package com.juan.holesfield.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.juan.holesfield.R
import com.juan.holesfield.model.Hole
import com.juan.holesfield.model.Progress

import com.juan.holesfield.utils.changeIntToColor
import com.juan.holesfield.viewmodel.HolesViewModel
import java.time.LocalDate
import java.util.*
import kotlin.concurrent.thread

class CreateHoleBottomFragment:BottomSheetDialogFragment(), View.OnClickListener{

    lateinit var crearTareaFull:FrameLayout
    lateinit var difFac:CardView
    lateinit var difMed:CardView
    lateinit var difDif:CardView
    lateinit var difImp:CardView
    lateinit var nombreTarea:TextInputEditText
    lateinit var resumenTarea:TextInputEditText
    lateinit var botonCrearTarea: MaterialButton

    override fun onClick(v: View?) {

        when(v!!.id){
            R.id.dificultadFacil ->     progress = Progress.NotStarted
            R.id.dificultadMedio ->     progress = Progress.Iniciated
            R.id.dificultadDificil ->   progress = Progress.Advanced
            R.id.dificultadImposible -> progress = Progress.Close
        }

        crearTareaFull.setBackgroundColor(changeIntToColor(this.context!!, Progress.colorProgress(progress)))
    }

    var progress: Progress = Progress.Unknown

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_create_hole_bottom, container, false)

        crearTareaFull = view!!.findViewById(R.id.crearTareaFull)

        nombreTarea = view.findViewById(R.id.nombreTareaCrear)
        resumenTarea = view.findViewById(R.id.resumenTareaCrear)

        difFac = view.findViewById(R.id.dificultadFacil)
        difFac.setOnClickListener(this)
        difMed = view.findViewById(R.id.dificultadMedio)
        difMed.setOnClickListener(this)
        difDif = view.findViewById(R.id.dificultadDificil)
        difDif.setOnClickListener(this)
        difImp = view.findViewById(R.id.dificultadImposible)
        difImp.setOnClickListener(this)

        botonCrearTarea = view.findViewById(R.id.crearNuevaTareaButton)
        botonCrearTarea.setOnClickListener { saveTarea() }

        return view
    }

    private fun saveTarea() {
        val nT = nombreTarea.text.toString()
        val rT = resumenTarea.text.toString()

        if(nT != "") {
            thread {
                val hole = Hole(
                        null,
                        hole = nT,
                        extra = rT,
                        progress = progress,
                        moment = LocalDate.now(),
                        completed = false,
                        completedMoment = null,
                        order = viewModel().getOrder() + 1
                )
                viewModel().addHole(hole)
                dismiss()
            }

        }
        else{
            Log.d("noCambioCOlor", "k")
            botonCrearTarea.text = "No"
            thread {
                Thread.sleep(1000)
                botonCrearTarea.text = "+"
            }
        }
    }

    fun viewModel():HolesViewModel = ViewModelProviders.of(activity!!).get(HolesViewModel::class.java)

    companion object {
        fun newInstance(): CreateHoleBottomFragment = CreateHoleBottomFragment()
    }
}