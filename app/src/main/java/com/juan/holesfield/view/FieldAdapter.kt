package com.juan.holesfield.view

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.juan.holesfield.R
import com.juan.holesfield.model.Hole
import com.juan.holesfield.model.Progress
import com.juan.holesfield.model.toString
import com.juan.holesfield.utils.DateUtils
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.concurrent.thread

class FieldAdapter(val context: Context,
                   val listener:clicky):
                RecyclerView.Adapter<FieldAdapter.FieldViewHolder>(), ItemTouchHelperAdapter {

    var holes: MutableList<Hole> = mutableListOf()
    var moving = false

    class FieldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var completHole: FrameLayout
        var foregroundHole: LinearLayout
        var backgroundHole: LinearLayout
        var holeName: TextView
        var holeExtra: TextView
        var progressChip: ProgressChip

        var extraProgress: View
        var holeDate: TextView
        var progress: RelativeLayout
        var expandButton: ImageView
        //var extra:LinearLayout


        init {
            completHole = itemView.findViewById(R.id.WholeHole)
            foregroundHole = itemView.findViewById(R.id.foregroundHole)
            backgroundHole = itemView.findViewById(R.id.backgroundHole)
            holeName = itemView.findViewById(R.id.holeName)
            holeExtra = itemView.findViewById(R.id.holeExtra)
            progressChip = itemView.findViewById(R.id.progressChip)

            extraProgress = itemView.findViewById(R.id.extraProgress)
            holeDate = itemView.findViewById(R.id.holeDate)
            progress = itemView.findViewById(R.id.progressHole)
            expandButton = itemView.findViewById(R.id.expandHole)
            //extra = itemView.findViewById(R.id.extraHole)

        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FieldViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_hole, parent, false)

        return FieldViewHolder(itemView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: FieldViewHolder, position: Int) {
        //TODO cambiar numeros por quiza colores para indicar progress

        if(position == holes.size) holder.completHole.visibility = View.INVISIBLE
        else {
            holder.completHole.visibility = View.VISIBLE
            val hole = holes[position]
            val dateUtils = DateUtils(context)
            val tiempoHole = hole.moment
            val color = ContextCompat.getColor(context, Progress.colorProgress(hole.progress))

            //if (hole.moment != LocalDate.now()) {
                val days = DateUtils.daysPassed(tiempoHole)
                holder.extraProgress.visibility = View.VISIBLE
                holder.holeDate.text = context.resources.getQuantityString(R.plurals.days, days, days)
            //} else holder.layoutInferiorfecha.visibility = View.GONE

            holder.progressChip.updateProgress(hole.progress)
            Log.d("FieldAdapter", "[$position] -> ${hole.progress.toString(context)}")
            Log.d("FieldAdapter", "inCustomView -> ${holder.progressChip.getProgress().toString(context)}")
            //TODO add a button to select progress
//            holder.progressChip.setOnCheckedChangeListener { _, isChecked ->
//                run { listener.progressChecked(isChecked, position, hole) }
//            }

            holder.progressChip

            val k = ((holder.progress.background) as GradientDrawable)
            k.setColor(color)
            holder.progress.background = k
            holder.backgroundHole.setBackgroundColor(color)

            holder.holeName.text = hole.hole
            if (hole.extra == "") holder.holeExtra.visibility = View.GONE
            else {
                holder.holeExtra.text = hole.extra
                holder.holeExtra.visibility = View.VISIBLE
            }

            holder.completHole.setOnClickListener { listener.holeClick(hole.id!!) }
        }
    }

    override fun getItemCount(): Int {
        //Log.d("numerodeHoles", numeroHoles.toString())
        return holes.size + 1
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Log.d("FieldAdapter", "initiatingMoving")
        Collections.swap(holes, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        moving = true
        listener.saveReordering(holes)
        Log.d("FieldAdapter", "finishedMoving")
        return true
    }

    fun updateList(it: MutableList<Hole>) {
        Log.d("FieldAdapter", "updating adapter")
        if(moving)
            moving = false
        else {
            holes = it
            notifyDataSetChanged()
        }
    }


}

interface ItemTouchHelperAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int):Boolean
}

interface clicky {
    fun holeClick(id:Long)
    fun progressChecked(checked: Boolean, position: Int, hole:Hole)
    fun saveReordering(holes:List<Hole>)
}