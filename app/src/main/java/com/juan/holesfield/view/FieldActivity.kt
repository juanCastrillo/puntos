package com.juan.holesfield.view

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.juan.holesfield.*
import com.juan.holesfield.model.Hole
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.juan.holesfield.model.Progress
import com.juan.holesfield.model.Progress.*
import com.juan.holesfield.utils.DateUtils
import com.juan.holesfield.utils.cammelCaseFormat
import com.juan.holesfield.utils.changeIntToColor
import com.juan.holesfield.viewmodel.HolesFactory
import com.juan.holesfield.viewmodel.HolesViewModel
import java.time.LocalDate
import kotlin.concurrent.thread

class FieldActivity:AppCompatActivity(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, View.OnClickListener {

    //regular views
    private lateinit var topImage: ImageView
    private lateinit var topDate:TextView
    private lateinit var fieldHoles: RecyclerView
    private lateinit var floatingButton: FloatingActionButton
    private lateinit var bottomSheetCreateHole:View
    private lateinit var bottomSheetBehavior:BottomSheetBehavior<View>

    //bottomsheet views
    lateinit var crearTareaFull:FrameLayout
    lateinit var cardNotStarted: CardView
    lateinit var cardInitiated: CardView
    lateinit var cardAdvanced: CardView
    lateinit var cardClose: CardView
    lateinit var nombreTarea: TextInputEditText
    lateinit var extraTarea: TextInputEditText

    var expanded = false//var createHoleBottomFragment: CreateHoleBottomFragment? = null
    var progress:Progress = Unknown

    val fm = supportFragmentManager
    private lateinit var inicioAdapter: FieldAdapter
    private lateinit var holesViewModel:HolesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_field)

        initActivityViews()
        initBottomSheetViews()
        holesViewModel = ViewModelProviders.of(this,HolesFactory(application, (application as Start).holesRepository))
                .get(HolesViewModel::class.java)

        holesViewModel.getHolesCustom().observe(this, androidx.lifecycle.Observer {
            Log.d("FieldActivity", "getHolesCustom: $it")
            inicioAdapter.updateList(it.toMutableList())
        })
        
        inicioAdapter = FieldAdapter(this, object:clicky{
            override fun saveReordering(holes: List<Hole>) {
                thread {holesViewModel.reOrderIndexes(holes)}
            }

            override fun progressChecked(checked: Boolean, position:Int, hole: Hole) {
                if(checked) {
                    hole.progress = Progress.mas(hole.progress)
                    thread { holesViewModel.updateProgress(hole) }
                    inicioAdapter.notifyItemChanged(position)
                }
            }

            override fun holeClick(id: Long) {
                openHole(id)
            }
        }); fieldHoles.adapter = inicioAdapter

        ItemTouchHelper(RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this, inicioAdapter)).attachToRecyclerView(fieldHoles)
    }

    private fun initActivityViews() {
        topDate = findViewById(R.id.topDate)
        topImage = findViewById(R.id.userPhoto)
        fieldHoles = findViewById(R.id.currentHolesList)
        floatingButton = findViewById(R.id.addHoleButton)
        bottomSheetCreateHole = findViewById(R.id.createHoleBottomSheet)

        fieldHoles.itemAnimator = DefaultItemAnimator()
        fieldHoles.layoutManager = LinearLayoutManager(this)

        topDate.text = DateUtils(this).fechaTextFormat()
        Picasso.get().load((application as Start).user!!.photoUrl).into(topImage)
        topImage.setOnClickListener { BottomFragment().show(fm, null) }
        floatingButton.setOnClickListener { floatingButtonClick() }

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetCreateHole)


        //changes the fab icon with the bottomsheet state
        bottomSheetBehavior.setBottomSheetCallback( object: BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet:View, newState:Int) {
                when(bottomSheetBehavior.state){
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        expanded = true
                        floatingButton.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ic_plus_24))
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        expanded = false
                        floatingButton.setImageDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.ic_outline_keyboard_arrow_up_24px))
                    }
                }
            }

            override fun onSlide(bottomSheet:View, slideOffset:Float) {
                Log.d("slideOffset", slideOffset.toString())
                //floatingButton.animate().translationX(slideOffset*translateDistance).translationY(slideOffset*floatingHistoryButton.height).setDuration(0).start()
            }
        })
    }

    private fun initBottomSheetViews(){
        crearTareaFull = findViewById(R.id.crearTareaFull)

        nombreTarea = findViewById(R.id.nombreTareaCrear)
        extraTarea = findViewById(R.id.resumenTareaCrear)

        cardNotStarted = findViewById(R.id.cardNotStarted)
        cardNotStarted.setOnClickListener(this)
        cardInitiated = findViewById(R.id.cardInitiated)
        cardInitiated.setOnClickListener(this)
        cardAdvanced = findViewById(R.id.cardAdvanced)
        cardAdvanced.setOnClickListener(this)
        cardClose = findViewById(R.id.cardClose)
        cardClose.setOnClickListener(this)

        allButtonsGrey()
    }

    private fun floatingButtonClick() {
        if(expanded){
            val nT = nombreTarea.text.toString()
            val rT = extraTarea.text.toString()

            if(nT != "") {
                thread {
                    val hole = Hole(
                            null,
                            hole = nT,
                            extra = rT,
                            progress = progress,
                            moment = LocalDate.now(),
                            completed = false,
                            completedMoment = null,
                            order = holesViewModel.getOrder() + 1
                    )
                    progress = Unknown
                    holesViewModel.addHole(hole)
                }

                resetBottomSheet()
            }
        }
        else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

    }

    private fun resetBottomSheet() {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        nombreTarea.text?.clear()
        extraTarea.text?.clear()
        allButtonsGrey()
    }

    override fun onClick(v: View) {

        when(v.id){
            R.id.cardNotStarted -> {
                progress = NotStarted
            }
            R.id.cardInitiated -> {
                progress = Iniciated
            }
            R.id.cardAdvanced -> {
                progress = Advanced
            }
            R.id.cardClose -> {
                progress = Close
            }
        }

        allButtonsGrey()

        Log.d("FiledActivity", "colorchanging")
        when(progress){
            NotStarted -> cardNotStarted.setCardBackgroundColor(ContextCompat.getColor(this, R.color.NotStarted))
            Iniciated -> cardInitiated.setCardBackgroundColor(ContextCompat.getColor(this, R.color.Iniciated))
            Advanced -> cardAdvanced.setCardBackgroundColor(ContextCompat.getColor(this, R.color.Advanced))
            Close -> cardClose.setCardBackgroundColor(ContextCompat.getColor(this, R.color.Close))
            Unknown -> {}
        }

        //crearTareaFull.setBackgroundColor(changeIntToColor(this.context!!, Progress.colorProgress(progress)))
    }

    private fun allButtonsGrey() {
        val greyColor = ContextCompat.getColor(this, R.color.unSelected)
        cardNotStarted.setCardBackgroundColor(greyColor)
        cardInitiated.setCardBackgroundColor(greyColor)
        cardAdvanced.setCardBackgroundColor(greyColor)
        cardClose.setCardBackgroundColor(greyColor)
        cardInitiated.setCardBackgroundColor(ColorStateList.valueOf(greyColor))
    }

    fun openHole(id:Long) {
        val intent = Intent(this, HoleActivity::class.java)
        intent.putExtra("holeId", id)
        startActivity(intent)
        Log.d("startOfAnimation", "k")
        animateThis()
        Log.d("endOfAnimation", "k")
    }

    private fun animateThis() {
        val a = AnimationUtils.loadAnimation(this, R.anim.fadein)
        a.reset()
        val ll = findViewById<View>(R.id.addHoleButton) as FloatingActionButton
        ll.clearAnimation()
        ll.startAnimation(a)
    }

    /**
     * swipe on the hole to delete it
     */
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        val hole = inicioAdapter.holes[position]
        hole.completed = true
        hole.order = 0
        hole.completedMoment = LocalDate.now()
        inicioAdapter.holes.removeAt(position)
        thread{
            holesViewModel.removeHole(hole)
            holesViewModel.reOrderIndexes(inicioAdapter.holes
            )}
        inicioAdapter.notifyItemRemoved(position)
    }

    fun mostrarHoles(holes: List<Hole>){
        holes.forEach {
            Log.d("hole", it.toString())
        }
    }

    override fun onBackPressed() {
        if(bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
            resetBottomSheet()
        else
            super.onBackPressed()
    }
}


