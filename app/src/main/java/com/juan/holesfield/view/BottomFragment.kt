package com.juan.holesfield.view

import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.squareup.picasso.Picasso
import com.juan.holesfield.R
import com.juan.holesfield.Start
import kotlin.reflect.KClass

class BottomFragment : BottomSheetDialogFragment(), View.OnClickListener {


    override fun onClick(v: View?) {
        when(v!!.id){

        }
    }

    private lateinit var menuGoogleUserName:TextView
    private lateinit var menuGoogleUserEmail:TextView
    private lateinit var menuGoogleUserImage: ImageView
    private lateinit var encuestasPress: LinearLayout
    private lateinit var respuestasAnterioresPress:LinearLayout
    private lateinit var settingsPress:LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_bottom, container, false)

        menuGoogleUserName = v.findViewById(R.id.menuGoogleUserName)
        menuGoogleUserEmail = v.findViewById(R.id.menuGoogleUserEmail)
        menuGoogleUserImage = v.findViewById(R.id.menuGoogleUserImage)

        encuestasPress = v.findViewById(R.id.Encuestas)
        encuestasPress.setOnClickListener(this)

        respuestasAnterioresPress = v.findViewById(R.id.respuestasAnteriores)
        respuestasAnterioresPress.setOnClickListener(this)

        settingsPress = v.findViewById(R.id.Ajustes)
        settingsPress.setOnClickListener(this)
        initMenu()
        return v
    }

    private fun Launch(ina: KClass<*>, ham_option_title:String) {
        val i = Intent(this.context, ina.java)
        i.putExtra("title", ham_option_title)
        //finish()  //Kill the activity from which you will go to next activity
        startActivityForResult(i, 0)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog

        if (dialog != null) {
            val bottomSheet = dialog.findViewById<View>(R.id.design_bottom_sheet)
            bottomSheet.layoutParams.height = convertDpToPixel(400F)
        }
        val view = view
        view!!.post {
            val parent = view.parent as View
            val params = parent.layoutParams as CoordinatorLayout.LayoutParams
            val behavior = params.behavior
            val bottomSheetBehavior = behavior as BottomSheetBehavior<*>?
            bottomSheetBehavior!!.peekHeight = view.measuredHeight

            parent.setBackgroundColor(Color.TRANSPARENT)
        }
    }

    private fun initMenu() {
        menuGoogleUserName.text = (activity!!.application as Start).user!!.displayName //nombre
        menuGoogleUserEmail.text = (activity!!.application as Start).user!!.email //mail*/
        //if(user.photoUrl != "")
        Picasso.get().load((activity!!.application as Start).user!!.photoUrl).into(menuGoogleUserImage)
    }

    fun convertDpToPixel(dp: Float): Int {
        val metrics = Resources.getSystem().displayMetrics
        val px = dp * (metrics.densityDpi / 270f)
        return Math.round(px)
    }

    companion object {
        fun newInstance(): BottomFragment = BottomFragment()
    }
}