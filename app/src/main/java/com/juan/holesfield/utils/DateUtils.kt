package com.juan.holesfield.utils

import android.content.Context
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*

class DateUtils(ct:Context) {
    private val format = DateTimeFormatter.ofPattern("EEEE d, MMMM yyyy", ct.resources.configuration.locales[0])
    fun fechaTextNoFormat() = format.format(LocalDateTime.now()).toString()
    fun fechaTextFormat() = cammelCaseFormat(fechaTextNoFormat())

    val holesFormat = DateTimeFormatter.ofPattern("d MMMM yyyy", ct.resources.configuration.locales[0])
    fun holesDateNow() = format.format(LocalDate.now())



    companion object {
        fun daysPassed(date:LocalDate):Int {
            val passed: Period = date.until(LocalDate.now())
            return passed.days
        }
        val archiveFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")
    }
}