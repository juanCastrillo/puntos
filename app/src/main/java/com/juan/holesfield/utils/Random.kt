package com.juan.holesfield.utils

import android.content.Context
import androidx.core.content.ContextCompat

fun changeIntToColor(ct: Context, colorInt: Int) = ContextCompat.getColor(ct, colorInt)

fun cammelCaseFormat(stringTemp: String): String {
    var string = ""
    var charAnt = ' '
    stringTemp.forEach {
        string += if(charAnt == ' ') it.toUpperCase()
        else               it
        charAnt = it
    }
    return string
}