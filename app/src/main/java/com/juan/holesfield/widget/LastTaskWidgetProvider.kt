package com.juan.holesfield.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.appwidget.AppWidgetProviderInfo
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import androidx.lifecycle.Observer
import androidx.room.Room
import com.juan.holesfield.R
import com.juan.holesfield.model.Hole
import com.juan.holesfield.storage.database.HolesDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LastTaskWidgetProvider: AppWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray?) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        val view = RemoteViews(context.packageName, R.layout.last_task_widget)
        GlobalScope.launch {
            val hole = getHole(context)
            view.setTextViewText(R.id.widgetTask_title, hole.hole)
            view.setTextViewText(R.id.widgetTask_extra, hole.extra)
        }
    }

    override fun onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager?, appWidgetId: Int, newOptions: Bundle?) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)

        Log.d("LastTaskWidgetProvider", "widget created")
        val view = RemoteViews(context.packageName, R.layout.last_task_widget)
        GlobalScope.launch {
            val hole = getHole(context)
            view.setTextViewText(R.id.widgetTask_title, hole.hole)
            view.setTextViewText(R.id.widgetTask_extra, hole.extra)
        }
    }

    private fun getHole(ct:Context): Hole {

        val view = RemoteViews(ct.packageName, R.layout.last_task_widget)
        val dao = Room.databaseBuilder(ct, HolesDatabase::class.java, "holes_database").build().holesDao()
        return dao.getLastHoleCustom()
    }
}

class LastTaskWidgetProviderInfo: AppWidgetProviderInfo() {
    override fun describeContents(): Int {
        return super.describeContents()
    }
}