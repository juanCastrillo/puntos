package com.juan.holesfield.storage.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.juan.holesfield.model.Hole

@Dao
interface HolesDao {

    @Query("SELECT * FROM holes order by progress")
    fun getHolesByProgress():LiveData<List<Hole>>

    @Query("SELECT * FROM holes order by moment")
    fun getHolesByMoment():LiveData<List<Hole>>

    @Query("SELECT * FROM holes order by custom_order")
    fun getHolesByCustomOrder():LiveData<List<Hole>>

    @Query("SELECT custom_order FROM holes order by custom_order DESC limit 1")
    fun getLastOrder():Int

    @Query("SELECT * FROM holes ORDER BY moment LIMIT 1")
    fun getLastHoleMoment():Hole

    @Query("SELECT * FROM holes ORDER BY custom_order LIMIT 1")
    fun getLastHoleCustom():Hole

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putIn(vararg holes: Hole)

    @Update
    fun updateIndexes(holes: List<Hole>)

    @Update
    fun updateProgress(hole:Hole)

    @Delete
    fun deleteHole(hole:Hole)
}