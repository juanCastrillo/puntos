package com.juan.holesfield.storage.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.juan.holesfield.model.Hole
import com.juan.holesfield.storage.Converters.LocalDateTimeConverter
import com.juan.holesfield.storage.Converters.ProgressConverter
import com.juan.holesfield.storage.dao.HolesDao

@Database(entities = [Hole::class], version = 1, exportSchema = false)
@TypeConverters(ProgressConverter::class, LocalDateTimeConverter::class)
abstract class HolesDatabase: RoomDatabase() {
    abstract fun holesDao():HolesDao
}