package com.juan.holesfield.storage.Converters

import androidx.room.TypeConverter
import com.juan.holesfield.utils.DateUtils
import java.time.LocalDate

class LocalDateTimeConverter {
    @TypeConverter
    fun LocalDateToString(date: LocalDate?):String? = if(date == null) null else DateUtils.archiveFormatter.format(date)

    @TypeConverter
    fun StringToLocalDate(date:String?): LocalDate? = if(date == null) null else LocalDate.parse(date, DateUtils.archiveFormatter)
}