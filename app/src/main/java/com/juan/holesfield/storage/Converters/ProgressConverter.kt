package com.juan.holesfield.storage.Converters

import androidx.room.TypeConverter
import com.juan.holesfield.model.Progress

class ProgressConverter {
    @TypeConverter fun convertToEntityProperty(puntos:Int): Progress = Progress.puntosToProgress(puntos)
    @TypeConverter fun convertToDatabaseValue(progress: Progress):Int = progress.puntos()
}